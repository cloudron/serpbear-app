### Overview

SerpBear is an Open Source Search Engine Position Tracking App. It allows you to track your website's keyword positions in Google and get notified of their positions.

#### Features

* Unlimited Keywords: Add unlimited domains and unlimited keywords to track their SERP.
* Email Notification: Get notified of your keyword position changes daily/weekly/monthly through email.
* SERP API: SerpBear comes with built-in API that you can use for your marketing & data reporting tools. 
* Keyword Research: Ability to research keywords and auto-generate keyword ideas from your tracked website's content by integrating your Google Ads test account. 
* Google Search Console Integration: Get the actual visit count, impressions & more for Each keyword. Discover new keywords, and find the most performing keywords, countries, and pages.
* Export CSV: Export your domain keywords and their data in CSV files whenever you want.
* Mobile App: Add the PWA app to your mobile for a better mobile experience.

