FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=towfiqi/serpbear versioning=semver extractVersion=^v(?<version>.+)$
ARG SERPBEAR_VERSION=2.0.7

RUN chown cloudron:cloudron /app/code
USER cloudron

RUN curl -L https://github.com/towfiqi/serpbear/archive/refs/tags/v${SERPBEAR_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code/

RUN npm install && \
    npm run build && \
    rm -rf node_modules/.cache data __tests__ __mocks__

# .env.local has precedence over .env
RUN ln -sf /run/serpbear/.env /app/code/.env.local && \
    ln -sf /app/data/data /app/code/data

USER root
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY start.sh /app/pkg/

CMD ["/app/pkg/start.sh"]
