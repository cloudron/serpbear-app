#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_USERNAME='admin';
    const ADMIN_PASSWORD='changeme123';
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/login');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//label[text()="Username"]/following-sibling::input'));
        await browser.findElement(By.xpath('//label[text()="Username"]/following-sibling::input')).sendKeys(username);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(text(), "Login")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//a[@href="/domains"]'));
    }

    async function logout() {
        await clearCache();
    }

    async function addDomain(domain) {
        await browser.get('https://' + app.fqdn + '/domains');
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//button[@data-testid="addDomainButton"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//div[@data-testid="adddomain_modal"]//textarea')).sendKeys(domain);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Add Domain"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//a[contains(., "' + domain.replace(/(^\w+:|^)\/\//, '')  + '")]'));
    }

    async function checkDomain(domain) {
        await browser.get('https://' + app.fqdn + '/domains');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//a[contains(., "' + domain.replace(/(^\w+:|^)\/\//, '')  + '")]'));
        await browser.findElement(By.xpath('//a[contains(., "' + domain.replace(/(^\w+:|^)\/\//, '')  + '")]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h1[contains(., "' + domain.replace(/(^\w+:|^)\/\//, '')  + '")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add domain', addDomain.bind(null, 'https://cloudron.io'));
    it('check check domain', checkDomain.bind(null, 'https://cloudron.io'));
    it('can logout', logout);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check check domain', checkDomain.bind(null, 'https://cloudron.io'));
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check check domain', checkDomain.bind(null, 'https://cloudron.io'));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check check domain', checkDomain.bind(null, 'https://cloudron.io'));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', async function () {
        execSync(`cloudron install --appstore-id com.serpbear.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add domain', addDomain.bind(null, 'https://cloudron.io'));
    it('check check domain', checkDomain.bind(null, 'https://cloudron.io'));
    it('can logout', logout);

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check check domain', checkDomain.bind(null, 'https://cloudron.io'));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

});
