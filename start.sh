#!/bin/bash
set -eu

echo "==> Creating directories"
mkdir -p /app/data/data /run/serpbear

waitForSerpbear() {
    # Wait for app to come up
    while ! curl --fail -s http://localhost:3000 > /dev/null; do
        echo "==> Waiting for app to come up"
        sleep 1
    done
}

function smtpSetup() {
    echo "==> Updating SMTP details"

    readonly secret=$(cat /app/data/env | sed -ne 's/SECRET="\(.*\)"/\1/p')
    readonly smtp_password=$(node -e "Cryptr = require('cryptr'); console.log(new Cryptr('${secret}').encrypt(process.env.CLOUDRON_MAIL_SMTP_PASSWORD))")
    cat /app/data/data/settings.json | \
        jq ".notification_email_from=\"${CLOUDRON_MAIL_FROM}\"" | \
        jq ".smtp_server=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" | \
        jq ".smtp_port=\"${CLOUDRON_MAIL_SMTP_PORT}\"" | \
        jq ".smtp_username=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" | \
        jq ".smtp_password=\"${smtp_password}\"" | \
        jq -r tostring | \
        sponge /app/data/data/settings.json
}

if [[ ! -f /app/data/env ]]; then
    secret=$(openssl rand -base64 32)
	api_key=$(openssl rand -base64 24)

    cat > /app/data/env << EOF
# Add custom environment variables in this file (https://docs.serpbear.com/miscellaneous/environment-variables)
USER=admin
PASSWORD=changeme123
SECRET="${secret}"
APIKEY="${api_key}"
EOF
fi

echo "==> Merge cloudron and custom configs"
cat > /run/serpbear/.env << EOF
NEXT_PUBLIC_LICENSE_CONSENT=true
SESSION_DURATION=24
NEXT_PUBLIC_APP_URL="${CLOUDRON_APP_ORIGIN}"
EOF
cat /app/data/env >> /run/serpbear/.env

# NOTE: any exported environment variables will end up overriding the env file!
export NODE_ENV=production

# sqlite3 does not allow multiple writes in WAL mode. This means that we cannot update settings in the background
# https://www.sqlite.org/wal.html (concurrency section, 3rd para)
if [[ ! -f "/app/data/data/database.sqlite" ]]; then
    echo "==> Creating database on first run"
    node /app/code/node_modules/.bin/next start &

    pid=$!
    sleep 5

    waitForSerpbear

    readonly endpoint=http://localhost:3000/api
    # login
    curl -X POST -c /tmp/cookies.txt "${endpoint}/login"  -H 'Content-Type: application/json' --data "{\"username\":\"admin\",\"password\":\"changeme123\"}"

    # get settings json
    default_settings=$(curl -sL -b /tmp/cookies.txt "${endpoint}/settings"  -H 'Content-Type: application/json')
    echo "==> Default settings: "
    echo ${default_settings} | jq ".settings" | jq -r tostring | sponge /app/data/data/settings.json
    cat /app/data/data/settings.json

    sleep 2

    echo "==> Stopping after database creating"
    kill -SIGTERM ${pid} # this kills the process group
fi

smtpSetup

echo "==> Migrate DB"
npm run db:migrate

echo "==> Ensure permissions"
chown -R cloudron:cloudron /app/data /run/serpbear /app/code/.next

echo "==> Starting SerpBear"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i SerpBear
