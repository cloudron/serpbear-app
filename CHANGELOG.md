[0.1.0]
* Initial version for SerpBear 2.0.2

[0.2.0]
* Enable scraper methods

[0.3.0]
* Fix cron setup

[0.4.0]
* Various packaging fixes

[1.0.0]
* Try to fix cron again

[1.0.1]
* Update serpbear to 2.0.4
* [Full Changelog](https://github.com/towfiqi/serpbear/releases/tag/v2.0.3)
* Ability to keywords for both mobile and desktop at once. (3786438), closes #60 #66 #199
* Adds ability to set Notification Email From name. (b35d333), closes #222
* Adds the ability to show hide columns in tracked keywords table. (d3e3760), closes #224
* auto filter keywords if they already exist instead of throwing error. (a09eb62), closes #244
* Displays Best position on mobile layout as well. (a74338f)
* Displays keyword's best position in email notification. (4c2f900)

[1.0.2]
* Update serpbear to 2.0.5
* [Full Changelog](https://github.com/towfiqi/serpbear/releases/tag/v2.0.3)

[1.0.3]
* Update serpbear to 2.0.6
* [Full Changelog](https://github.com/towfiqi/serpbear/releases/tag/v2.0.3)

[1.0.4]
* Update serpbear to 2.0.7
* [Full Changelog](https://github.com/towfiqi/serpbear/releases/tag/v2.0.7)
* Resolves AdWords integration issue. (36ed4cf)
* Fixes broken CDN images. (bf911b4)
* Add error message, if returned search HTML does not contain required elements by @phoehnel in #273

